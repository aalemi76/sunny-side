//
//  WeatherListViewController.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import UIKit
import RxSwift
import CoreLocation
import Network

class WeatherListViewController: SharedViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    let viewModel: ViewModelProvider
    
    let disposeBag = DisposeBag()
    
    var location = CLLocationCoordinate2D()
    
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "InternetConnectionMonitor")
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .plain)
        tbl.backgroundColor = .none
        tbl.tableHeaderView = nil
        tbl.tableFooterView = nil
        tbl.sectionHeaderHeight = 0
        tbl.sectionFooterHeight = 0
        tbl.separatorStyle = .none
        tbl.rowHeight = UITableView.automaticDimension
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        return container
    }()
    
    var locationManager = CLLocationManager()
    
    init(viewModel: WeatherListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkForInternetConnection()
        viewModel.viewDidLoad(self)
        addTableView()
        configureLocationManager()
    }
    
    func addTableView() {
        tableView.backgroundColor = .white
        addBGToTableView()
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    func addBGToTableView() {
        let bgImage = UIImageView(image: UIImage(named: "Image"))
        bgImage.contentMode = .scaleAspectFill
        bgImage.clipsToBounds = true
        let blurEffect = UIBlurEffect(style: .dark)
        let visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.alpha = 1
        bgImage.addSubview(visualEffectView)
        visualEffectView.pinToEdge(bgImage)
        tableView.backgroundView = bgImage
    }
    
    func configureLocationManager() {
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func getUserCity() {
        let alertController = UIAlertController(title: "Enter city name", message: nil, preferredStyle: .alert)
        alertController.addTextField()
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned alertController] _ in
            guard let city = alertController.textFields?[0].text else { return }
            (self.viewModel as? WeatherListViewModel)?.addCity(cityName: city)
        }
        
        alertController.addAction(submitAction)
        
        present(alertController, animated: true)
    }
    
    func checkForInternetConnection() {
        
        monitor.pathUpdateHandler = { [unowned self] pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                if (viewModel as? WeatherListViewModel)?.cityCells.count == 0 {
                    location = CLLocationCoordinate2D()
                    locationManager.startUpdatingLocation()
                }
            } else {
                showErrorBanner(title: "No internet connection.")
            }
        }
        
        monitor.start(queue: queue)
        
    }
    
}
