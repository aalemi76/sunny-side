//
//  WeatherListViewController+Ext.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation
import CoreLocation

extension WeatherListViewController: Viewable {
    
    func show(result: Result<Any, SSError>) {
        DispatchQueue.main.async { [weak self] in
            
            switch result {
                
            case .success(let sections):
                
                guard let sections = sections as? [Sectionable] else {
                    self?.showErrorBanner(title: "There was an error while loading the list.")
                    return
                }
                
                self?.showSuccessBanner(title: "Successfully loaded data.")
                self?.configureTableView(sections)
                
            case .failure(let error):
                
                self?.showErrorBanner(title: error.rawValue)
            }
            
        }
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        subscribeOnTap()
    }
    
    func subscribeOnTap() {
        tableViewContainer.delegateHandler.passSelectedItem.subscribe { [weak self] (event) in
            
        }.disposed(by: disposeBag)
    }
    
}

extension WeatherListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location else { return }
        if abs(location.coordinate.latitude - self.location.latitude) > 1 {
            self.location = location.coordinate
            fetchCityAndCountry(from: location) { [weak self] city, country, error in
                (self?.viewModel as? WeatherListViewModel)?.city = city ?? ""
            }
        }
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
    
}
