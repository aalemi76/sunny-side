//
//  AddCityTableViewCell.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import UIKit

class AddCityTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID: String = String(describing: AddCityTableViewCell.self)
    
    @IBOutlet weak var celciusButton: UIButton!
    @IBOutlet weak var farenheitButton: UIButton!
    @IBOutlet weak var addCityButton: UIButton!
    
    var viewModel: Reusable?
    
    @objc func addButtonDidTap() {
        (viewModel as? AddCityCellViewModel)?.delegate?.addButtonDidTap()
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
    }
    
    func update(model: Any) {
        return
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        addCityButton.addTarget(self, action: #selector(addButtonDidTap), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
