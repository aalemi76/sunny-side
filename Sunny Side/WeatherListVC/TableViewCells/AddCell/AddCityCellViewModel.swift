//
//  AddCityCellViewModel.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

protocol AddCityCellViewModelDelegate: AnyObject {
    func addButtonDidTap()
}

class AddCityCellViewModel: TableCellViewModel {
    
    weak var delegate: AddCityCellViewModelDelegate?
    
}
