//
//  CityTableViewCell.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import UIKit

class CityTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID: String = String(describing: CityTableViewCell.self)

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var lowTempLabel: UILabel!
    @IBOutlet weak var highTempLabel: UILabel!
    
    var viewModel: Reusable?
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
    }
    
    func update(model: Any) {
        guard let model = model as? WeatherModel else { return }
        cityNameLabel.text = model.name
        tempLabel.text = "\(Int(model.main.temp))°"
        weatherDescriptionLabel.text = model.weather.first?.weatherDescription
        lowTempLabel.text = "\(Int(model.main.tempMin))°"
        highTempLabel.text = "\(Int(model.main.tempMax))°"
        return
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
