//
//  WeatherListViewModel+Ext.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

extension WeatherListViewModel: AddCityCellViewModelDelegate {
    
    func addButtonDidTap() {
        (view as? WeatherListViewController)?.getUserCity()
    }
    
}
