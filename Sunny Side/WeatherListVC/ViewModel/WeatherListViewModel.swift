//
//  WeatherListViewModel.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

class WeatherListViewModel: ViewModelProviderMappable {
    
    typealias model = WeatherModel
    
    let interactor: Interactor<WeatherModel>
    
    weak var view: Viewable?
    
    var city: String = "" {
        didSet {
            fetchItems()
        }
    }
    
    var cityCells = [Reusable]()
    
    var sections = [Sectionable]()
    
    required init(interactor: Interactor<WeatherModel>) {
        self.interactor = interactor
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        createCityWeatherSection()
        createAddCitySection()
        city = "Vancouver"
    }
    
    func fetchItems() {
        interactor.getModel(city: city, headers: nil) { [weak self] model in
            self?.createCurrentLocation(model: model)
            self?.view?.show(result: .success(self?.sections))
        } onFailure: { error in
            self.view?.show(result: .failure(error))
        }

        return
    }
    
    func addCity(cityName: String) {
        interactor.getModel(city: cityName, headers: nil) { [weak self] model in
            self?.addCityToList(model: model)
            self?.view?.show(result: .success(self?.sections))
        } onFailure: { error in
            self.view?.show(result: .failure(error))
        }
    }
    
    func createCurrentLocation(model: model) {
        let cell = CityCellViewModel(reuseID: CityTableViewCell.reuseID, cellClass: CityTableViewCell.self, model: model)
        if cityCells.count > 0 {
            cityCells.remove(at: 0)
        }
        cityCells.insert(cell, at: 0)
        let section = sections[0]
        section.removeCells()
        section.append(cityCells)
    }
    
    func addCityToList(model: model) {
        let cell = CityCellViewModel(reuseID: CityTableViewCell.reuseID, cellClass: CityTableViewCell.self, model: model)
        cityCells.append(cell)
        let section = sections[0]
        section.append([cell])
    }
    
    func createCityWeatherSection() {
        let section = SectionProvider(cells: cityCells, headerView: nil, footerView: nil)
        sections.insert(section, at: 0)
    }
    
    func createAddCitySection() {
        let cell = AddCityCellViewModel(reuseID: AddCityTableViewCell.reuseID, cellClass: AddCityTableViewCell.self, model: "")
        cell.delegate = self
        let section = SectionProvider(cells: [cell], headerView: nil, footerView: nil)
        sections.append(section)
    }
    
}
