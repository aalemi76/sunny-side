//
//  WeatherListCoordinator.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import UIKit

class WeatherListCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = [Coordinator]()
    }
    
    func start() {
        let viewModel = WeatherListViewModel(interactor: Interactor<WeatherModel>())
        let vc = WeatherListViewController(viewModel: viewModel)
        push(viewController: vc, animated: true)
    }
    
    
}
