//
//  SSError.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

enum SSError: String, Error {
    case serverResponse = "Unable to communicate with server, please check your connection and try again."
    case parsingError = "Unable to parse received data to the specified model."
    case couldnotFindTheCity = "Unable to find the city. Please double check your spelling."
}
