//
//  SSNavigationController.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import UIKit

class SSNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = GlobalSettings.shared().mainColor
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navigationBar.tintColor = .white
            navigationBar.standardAppearance = appearance
            navigationBar.compactAppearance = appearance
            navigationBar.scrollEdgeAppearance = appearance
        } else {
            configureView()
        }
        setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func configureView() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: GlobalSettings.shared().systemFont(type: .bold, size: 18)]
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.barTintColor = GlobalSettings.shared().mainColor
        navigationBar.tintColor = .white
        navigationBar.shadowImage = UIImage()
    }

}
