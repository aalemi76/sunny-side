//
//  ViewModelProviderMappable.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

protocol ViewModelProviderMappable: ViewModelProvider {
    associatedtype model: Codable
    init(interactor: Interactor<model>)
}
