//
//  ViewModelProvider.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

protocol ViewModelProvider {
    func viewDidLoad(_ view: Viewable)
    func fetchItems()
}
