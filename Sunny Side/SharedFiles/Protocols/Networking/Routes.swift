//
//  Routes.swift
//  Placebo
//
//  Created by AliReza on 2022-05-11.
//

import Foundation

enum Routes: String {
    
    case appid = "a567eca4b52f20847650b5f73af13af9"
    case baseURL = "https://api.openweathermap.org/data/2.5"
    case weather = "/weather"
    
    static func generate(_ route: Routes) -> String {
        return Routes.baseURL.rawValue + route.rawValue
    }

}
