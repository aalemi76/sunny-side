//
//  Units.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

enum Units: String {
    
    case standard = "standard"
    case metric = "metric"
    case imperial = "imperial"
    
}
