//
//  Interactor.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation
import Alamofire

class Interactor<Model: Codable> {
    
    typealias Object = Model
    
    private func makeParameters(city: String, mode: Mode, unit: Units) -> [String: String] {
        
        var params = [String: String]()
        params = ["q": city, "appid": Routes.appid.rawValue,
                  "mode": mode.rawValue, "units": unit.rawValue]
        
        return params
    }
    
    func getModel(url: String? = nil, city: String, mode: Mode = .json, unit: Units = .metric, headers: [String: String]?, onSuccess: @escaping (Object) -> Void, onFailure: @escaping (SSError) -> Void) {
        
        let params = makeParameters(city: city, mode: mode, unit: unit)
        
        var urlString = ""
        if let url = url {
            urlString = url
        } else {
            urlString = Routes.generate(.weather)
        }
        
        AF.request(urlString, method: .get, parameters: params, encoding: URLEncoding.queryString, interceptor: nil, requestModifier: nil).responseDecodable(of: Model.self, queue: .global(qos: .background)) { response in
            if response.response?.statusCode == 404 {
                onFailure(.couldnotFindTheCity)
                return
            }
            guard let object = response.value else {
                onFailure(.parsingError)
                return
            }
            
            onSuccess(object)
            
        }
        
    }
}
