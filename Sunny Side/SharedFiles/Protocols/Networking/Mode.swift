//
//  Mode.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation

enum Mode: String {
    
    case xml = "xml"
    case html = "html"
    case json = "JSON"
    
}
