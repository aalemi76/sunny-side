//
//  Viewable.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation
protocol Viewable: AnyObject {
    func show(result: Result<Any, SSError>)
    func loadNextPage(result: Result<Any, SSError>)
}

extension Viewable {
    func loadNextPage(result: Result<Any, SSError>) {
        return
    }
}
