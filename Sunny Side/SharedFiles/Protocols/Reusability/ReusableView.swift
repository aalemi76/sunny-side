//
//  ReusableView.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation
protocol ReusableView {
    func getReuseID() -> String
    func getViewClass() -> AnyClass
}
