//
//  Updatable.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation
protocol Updatable: AnyObject {
    func attach(viewModel: Reusable)
    func update(model: Any)
}
