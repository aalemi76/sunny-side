//
//  Reusable.swift
//  Sunny Side
//
//  Created by AliReza on 2022-08-25.
//

import Foundation
protocol Reusable: AnyObject {
    init(reuseID: String, cellClass: AnyClass, model: Any)
    func getReuseID() -> String
    func getCellClass() -> AnyClass
    func getModel() -> Any
    func updateModel(model: Any)
    func cellDidLoad(_ cell: Updatable)
}
