//
//  InteractorTests.swift
//  Sunny SideTests
//
//  Created by AliReza on 2022-08-26.
//

import XCTest
@testable import Sunny_Side

class InteractorTests: XCTestCase {
    
    var interactor: Interactor<WeatherModel>!

    override func setUpWithError() throws {
        interactor = Interactor<WeatherModel>()
    }

    override func tearDownWithError() throws {
        interactor = nil
    }
    
    func testCityNameEquality() throws {
        let city = "Vancouver"
        interactor.getModel(city: city, headers: nil) { model in
            XCTAssertEqual(model.name, city)
        } onFailure: { error in
            XCTAssertEqual(error, SSError.couldnotFindTheCity)
        }

    }
    
    func testCityNotFound() throws {
        let city = "Newyork"
        interactor.getModel(city: city, headers: nil) { model in
            XCTAssertEqual(model.name, city)
        } onFailure: { error in
            XCTAssertEqual(error, SSError.couldnotFindTheCity)
        }

    }
    
    func testTempConsistencyDegree() throws {
        let city = "Vancouver"
        interactor.getModel(city: city, headers: nil) { model in
            XCTAssertLessThan(model.main.temp, 40)
        } onFailure: { error in
            XCTAssertEqual(error, SSError.couldnotFindTheCity)
        }
    }

    func testNetworkingPerformance() throws {
        self.measure {
            let city = "Vancouver"
            interactor.getModel(city: city, headers: nil) { _ in
                
            } onFailure: { _ in
                
            }
        }
    }

}
