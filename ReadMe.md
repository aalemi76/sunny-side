# Sunny Side

## Overview
<img src="https://img.shields.io/badge/Platform-iOS-blueviolet">
<img src="https://img.shields.io/badge/Language-Swift-important">
<img src="https://img.shields.io/badge/iOS-12%2B-yellow">


Within this application you can see weather information of different cities. Below you can see a snapshot of the application. The first row shows curent weather based on your current location. By tapping on plus sign you can search for your desired city and add its weather information to the list.

<img src="appScreenshot.png" width="332.70" height="720">

## Architecture

Following diagram shows the implemented MVVM-C architecture. The coordinator entity takes care of navigating between applications different ViewControllers. ViewModel entity, as the name suggests, takes the responsibility of passing recieved data from Interactor to ViewController. The Interactor itself is a generic class whose task is to get data from API server and convert it to a specified model.

![alt text](appArch.jpeg)

## 3rd party libraries
- [Alamofire](https://github.com/Alamofire/Alamofire)
- [RxSwift](https://github.com/ReactiveX/RxSwift)

## Challenges

There are few note worthy challenges which a developer may face. One of the important issues during application development is to maintain application architecture within different files and classes. According to the implemented architecture a table view cell itself cannot present an alert controller to get user desired city. It should be done by the view model. By using delegate pattern the problem has been solved. 

The other problem is user internet connection. We chould observe on the connection changes and if the connection is available we should interact with server to recieve weather data. By defining an observer on internet connection of the device we can perceive whether the user is connected and if so call the proper method inside the class.

## Further Improvements

For the main page we can add a feature to convert temperatures from °C to °F and vice versa. Also we should persist user added city in local storage of the device. Currently the app does not support any kind of data persistency.

We can also have a weather detail page for every city in the list. We can have lots of new information about the city's weather in this page.

